package org.jboss.gwt.elemento.sample.errai.client;

import elemental2.dom.EventListener;
import elemental2.dom.Text;

import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.collapsible.Collapsible;
import org.dominokit.domino.ui.icons.Icon;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.modals.ModalDialog;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.style.Styles;
import org.dominokit.domino.ui.tree.Tree;
import org.dominokit.domino.ui.tree.TreeItem;
import org.jboss.errai.ui.nav.client.local.Navigation;

import javax.inject.Inject;

public class MainMenu {

    @Inject Navigation navigation;

    private Tree menu;
    private Icon lockIcon = Icons.ALL.lock_open()
            .setColor(Color.GREY)
            .style()
            .setMarginBottom("0px")
            .setMarginTop("0px")
            .setCursor("pointer")
            .add(Styles.pull_right)
            .get();
    private boolean locked = false;
    private Collapsible lockCollapsible = Collapsible.create(lockIcon).expand();


    public void setTitle(String title) {
        menu.getTitle().setTextContent(title);
    }

    public Tree init() {
        menu = Tree.create("Demo menu");
        menu.getHeader().appendChild(lockIcon.asElement());

        menu.enableSearch()
                .autoExpandFound()
                .style()
                .setHeight("calc(100vh - 267px)").get();

        lockIcon.addClickListener(evt -> {
            if (locked) {
                lockIcon.asElement().textContent = Icons.ALL.lock().getName();
                ModalDialog smallSizeModal = createModalDialog().small();
                smallSizeModal.toggleDisplay();
                locked = false;
            } else {
                lockIcon.asElement().textContent = Icons.ALL.lock_open().getName();
                ModalDialog smallSizeModal = createModalDialog().small();
                smallSizeModal.toggleDisplay();
                locked = true;
            }
        });
        return menu;
    }

    public TreeItem addMenuItem(String title, String iconName) {
        TreeItem menuItem = TreeItem.create(title, Icon.create(iconName));
        menu.appendChild(menuItem);
        menuItem.addClickListener(e -> {
            navigation.goTo(title);
        });
        return menuItem;
    }

    public TreeItem addMenuItem(String title, Icon icon) {
        TreeItem menuItem = TreeItem.create(title, icon);
        menu.appendChild(menuItem);
        menuItem.addClickListener(e -> {
            navigation.goTo(title);
        });
        return menuItem;
    }

    private ModalDialog createModalDialog() {
        ModalDialog modal = ModalDialog.create("Modal title");
        modal.appendChild(new Text("SAMPLE_CONTENT"));
        Button closeButton = Button.create("CLOSE").linkify();
        Button saveButton = Button.create("SAVE CHANGES").linkify();

        EventListener closeModalListener = evt -> modal.close();

        closeButton.addClickListener(closeModalListener);
        saveButton.addClickListener(closeModalListener);
        modal.appendFooterChild(saveButton);
        modal.appendFooterChild(closeButton);
        return modal;
    }

}
