package org.jboss.gwt.elemento.sample.errai.client;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import elemental2.dom.*;
import org.apache.http.annotation.Obsolete;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.datatable.ColumnConfig;
import org.dominokit.domino.ui.datatable.DataTable;
import org.dominokit.domino.ui.datatable.TableConfig;
import org.dominokit.domino.ui.datatable.store.LocalListDataStore;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Columns;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.grid.Row_12;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.layout.Layout;
import org.dominokit.domino.ui.style.ColorScheme;
import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.common.client.api.ErrorCallback;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.ioc.client.api.EntryPoint;
import org.jboss.errai.ui.nav.client.local.ContentDelegation;
import org.jboss.errai.ui.nav.client.local.NavigatingContainer;
import org.jboss.errai.ui.nav.client.local.Navigation;
import org.jboss.errai.ui.nav.client.local.NavigationPanel;
import org.jboss.errai.ui.nav.client.local.api.DelegationControl;
import org.jboss.gwt.elemento.core.Elements;
import org.jboss.gwt.elemento.core.Widgets;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.jboss.gwt.elemento.core.Elements.p;

@EntryPoint
public class Main {
    @Inject private Navigation navPanel;
    @Inject MainMenu menu;
    @Inject private Caller<TestRest> testRestCaller;

    private Row_12 tableContainer = Row.create(Columns._12);
    @Inject
    private FooterElement footer;


    @PostConstruct
    void init() {

        DominoNavigation.layout.getLeftPanel().appendChild(menu.init());
        DominoNavigation.layout.showFooter();
        DominoNavigation.layout.getFooter().appendChild(p().textContent("© 2018 Copyright DominoKit"));
        DominoNavigation.layout.show(ColorScheme.LIGHT_BLUE);

        DomGlobal.console.info("showing layout  --------------->");

        navPanel.setContentDelegation(new ContentDelegation() {
            @Override
            public void showContent(Object page, NavigatingContainer defaultContainer, IsWidget widget, Object previousPage, DelegationControl control) {
                DominoNavigation.layout.getContentPanel().appendChild(Widgets.asElement(widget));
                control.proceed();
            }

            @Override
            public void hideContent(Object page, NavigatingContainer defaultContainer, IsWidget widget, Object nextPage, DelegationControl control) {
                DominoNavigation.layout.getContentPanel().clearElement();
                control.proceed();

            }
        });

        Button button = Button.createPrimary("Get users from rest server");
//        Elements.body().add(button);
//        Elements.body().add(tableContainer);




        menu.addMenuItem(MainPage.class.getSimpleName(), Icons.ACTION_ICONS.android());
        menu.addMenuItem(Test1Page.class.getSimpleName(), "account_box");
        menu.addMenuItem(UserPage.class.getSimpleName(), Icons.ACTION_ICONS.accessibility());
        //layout.getContentPanel().appendChild(button);
        //layout.getContentPanel().appendChild(tableContainer);

        //Node mainContent = navigation.getContentPanel();
        //layout.getContentPanel().appendChild(Widgets.asElement(navPanel.getContentPanel().asWidget()));
        //Elements.body().add(Widgets.asElement(navPanel.getContentPanel().asWidget()));
        //layout.getContentPanel().appendChild(new Button("END"));



        History.fireCurrentHistoryState();
        RestClient.setJacksonMarshallingActive(true);
        RestClient.setApplicationRoot("https://jsonplaceholder.typicode.com/");

        RemoteCallback<List<User>> callback = users -> buildTable(users);
        ErrorCallback<String> errorCalback = (s, throwable) -> {
            Window.alert("rest communication fail");
            return true;
        };
        button.addClickListener(evt -> testRestCaller.call(callback, errorCalback).getUsers(""));




    }

    private void buildTable(List<User> users) {
        LocalListDataStore<User> localListDataStore = new LocalListDataStore<>();
        TableConfig<User> tableConfig = new TableConfig<>();
        tableConfig
                .addColumn(ColumnConfig.<User>create("id", "#")
                        .textAlign("right")
                        .asHeader()
                        .setCellRenderer(cell -> new Text(cell.getTableRow().getRecord().getId() + 1 + "")))

                .addColumn(ColumnConfig.<User>create("name", "name")
                        .setCellRenderer(cell -> new Text(cell.getTableRow().getRecord().getName())))

                .addColumn(ColumnConfig.<User>create("username", "User name")
                        .setCellRenderer(cell -> new Text(cell.getTableRow().getRecord().getUsername())));

        localListDataStore.setData(users);
        DataTable<User> table = new DataTable<>(tableConfig, localListDataStore);
        tableContainer.appendChild(Column.span12().appendChild(table));
        table.load();
    }

}
