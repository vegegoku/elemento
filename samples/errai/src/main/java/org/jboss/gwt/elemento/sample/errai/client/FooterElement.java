/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jboss.gwt.elemento.sample.errai.client;

import javax.inject.Singleton;

import elemental2.dom.HTMLElement;
import org.jboss.gwt.elemento.core.IsElement;
import org.jboss.gwt.elemento.sample.common.Urls;

import static org.jboss.gwt.elemento.core.Elements.*;

@Singleton
@SuppressWarnings("CdiInjectionPointsInspection")
public class FooterElement implements IsElement<HTMLElement> {

    private final HTMLElement root;

    public FooterElement() {
        root = footer().css("info")
                .add(hr())
                .add(
                    p().add(span().textContent("Created By Daniel Korbel"))
                        .add(p().add(span().textContent("Created with: Domino UI ")).add(a("https://dominokit.github.io/domino-ui-demo/index.html?theme=indigo#home").textContent("Errai DominoUi Demo")))
                        .add(p().add(span().textContent("Created with: Errai Framework ")).add(a("http://erraiframework.org/").textContent("Errai Framework")))
                )
                .asElement();
    }

    @Override
    public HTMLElement asElement() {
        return root;
    }
}
