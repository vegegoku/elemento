package org.jboss.gwt.elemento.sample.errai.client;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;
import com.google.gwt.user.client.ui.Widget;
import elemental2.dom.HTMLElement;
import jsinterop.base.Js;

public class ContainerWidget extends Widget {
    public Widget parent;

    public ContainerWidget(HTMLElement element) {
        this.setElement(Element.as((Node)Js.cast(element)));
    }

    public void setParent(Widget parent){
        this.parent = parent;
    }

    @Override
    public Widget getParent() {
        return parent;
    }
}
