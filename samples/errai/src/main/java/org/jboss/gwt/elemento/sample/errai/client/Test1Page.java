package org.jboss.gwt.elemento.sample.errai.client;

import com.google.gwt.user.client.Window;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.dialogs.MessageDialog;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.infoboxes.InfoBox;
import org.dominokit.domino.ui.notifications.Notification;
import org.dominokit.domino.ui.style.Color;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageHiding;
import org.jboss.errai.ui.nav.client.local.PageShown;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;
import org.jboss.gwt.elemento.core.IsElement;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import static org.jboss.gwt.elemento.core.Elements.h;

@Templated(value = "template.html")
@Page(path = "Test1Page")
public class Test1Page implements IsElement {
    @Inject
    @DataField
    HTMLDivElement content;

    @PostConstruct
    private void preparePage() {
        content.appendChild(Row.create()
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.shopping_cart(), "NEW ORDERS", "125")
                        .setIconBackground(Color.RED)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.face(), "NEW MEMBERS", "257")
                        .setIconBackground(Color.INDIGO)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.shopping_cart(), "BOOKMARKS", "117")
                        .setIconBackground(Color.PURPLE)))
                .addColumn(Column.span3().appendChild(InfoBox.create(Icons.ALL.favorite(), "LIKES", "1432")
                        .setIconBackground(Color.DEEP_PURPLE)))
                .asElement());
    }

    @PageHiding
    private void unpreparePage() {
    }

    @Override
    public HTMLElement asElement() {
        return content;
    }
}
