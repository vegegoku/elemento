package org.jboss.gwt.elemento.sample.errai.client;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import elemental2.dom.DomGlobal;
import org.dominokit.domino.ui.layout.Layout;
import org.jboss.errai.ui.nav.client.local.NavigatingContainer;
import org.jboss.gwt.elemento.core.Widgets;

public class DominoNavigation implements NavigatingContainer {

    public static final Layout layout = Layout.create("ERRAI");
    private final ContainerWidget widget = new ContainerWidget(layout.getContentPanel().asElement());

    public DominoNavigation() {
        widget.setParent(Widgets.asWidget(DomGlobal.document.body));
    }

    @Override
    public void clear() {
        layout.getContentPanel().clearElement();
    }

    @Override
    public Widget getWidget() {
        return widget;
    }

    @Override
    public void setWidget(Widget w) {
        clear();
        layout.getContentPanel().appendChild(Widgets.asElement(w));
    }

    @Override
    public void setWidget(IsWidget w) {
        clear();
        layout.getContentPanel().appendChild(Widgets.asElement(w));
    }

    @Override
    public Widget asWidget() {
        return widget;
    }
}
