package org.jboss.gwt.elemento.sample.errai.client;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import java.util.List;

@Path("/users")
public interface TestRest {
    
    @GET
    public List<User> getUsers(String id);
    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg);
}
