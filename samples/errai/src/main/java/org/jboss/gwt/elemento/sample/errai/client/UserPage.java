package org.jboss.gwt.elemento.sample.errai.client;

import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.Typography.Paragraph;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.datepicker.DatePicker;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.header.BlockHeader;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.infoboxes.InfoBox;
import org.dominokit.domino.ui.notifications.Notification;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.style.ColorScheme;
import org.dominokit.domino.ui.style.Styles;
import org.dominokit.domino.ui.tabs.Tab;
import org.dominokit.domino.ui.tabs.TabsPanel;
import org.gwtproject.i18n.client.DateTimeFormat;
import org.gwtproject.i18n.client.impl.cldr.DateTimeFormatInfoImpl_fr;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageHiding;
import org.jboss.errai.ui.nav.client.local.PageShown;
import org.jboss.errai.ui.shared.api.annotations.DataField;
import org.jboss.errai.ui.shared.api.annotations.Templated;
import org.jboss.gwt.elemento.core.IsElement;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Date;

import static org.jboss.gwt.elemento.core.Elements.b;

@Templated(value = "template.html")
@Page(path = "UserPage")
public class UserPage implements IsElement {
    @Inject
    @DataField
    HTMLDivElement content;

    @PostConstruct
    private void preparePage() {
        content.appendChild(Card.create("EXAMPLE TAB", "Add quick, dynamic tab functionality to transition through panes of local content")
                .appendChild(TabsPanel.create()
                        .appendChild(Tab.create("HOME")
                                .appendChild(b().textContent("Home Content"))
                                .appendChild(Paragraph.create("SAMPLE_TEXT")))
                        .appendChild(Tab.create("PROFILE")
                                .appendChild(b().textContent("Profile Content"))
                                .appendChild(Paragraph.create("SAMPLE_TEXT")))
                        .appendChild(Tab.create("MESSAGES")
                                .appendChild(b().textContent("Messages Content"))
                                .appendChild(Paragraph.create("SAMPLE_TEXT"))
                                .activate())
                        .appendChild(Tab.create("SETTINGS")
                                .appendChild(b().textContent("Settings Content"))
                                .appendChild(Paragraph.create("SAMPLE_TEXT")))
                )
                .asElement());
    }

    @PageHiding
    private void unpreparePage() {
    }

    @Override
    public HTMLElement asElement() {
        return content;
    }
}
