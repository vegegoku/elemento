package org.jboss.gwt.elemento.sample.errai.client;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class User {

 private Long id;
 private String name;
 private String username;
 private String email;
 private String phone;
 private String website;

 public Long getId () {
     return id;
 }
 
 public void setId (Long id) {
     this.id=id;
 }
 
public String getName() {
   return name;
}
public void setName() {
     this.name = name;
}

public String getUsername() {
    return username;
}
public void setUsername(String username) {
this.username=username;
}

public String toString () {
    return "User: " + id + " " + name + " " + username;
}


}
